//
//  ScoreResponse.swift
//  CMS-mvp
//
//  Created by Mahamudul on 29/3/21.
//

import Foundation


// MARK: - ScoreResponse
struct ScoreResponse: Decodable {
    let success: Int?
    let myData: [MyDatum]?

    enum CodingKeys: String, CodingKey {
        case success = "success"
        case myData = "my_data"
    }
}

// MARK: - MyDatum
struct MyDatum: Decodable {
    let playedWith, location, startTime, date: String?
    let maxData: Int?
    let timePeriod: [TimePeriod]?

    enum CodingKeys: String, CodingKey {
        case playedWith = "Played with"
        case location = "Location"
        case startTime = "Start time"
        case date = "Date"
        case maxData = "max_data"
        case timePeriod = "time_period"
    }
}

// MARK: - TimePeriod
struct TimePeriod: Decodable {
    let dataTime, data: String?

    enum CodingKeys: String, CodingKey {
        case dataTime = "data_time"
        case data = "data"
    }
}
