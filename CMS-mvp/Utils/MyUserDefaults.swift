//
//  DefaultKeys.swift
//  SingFit-Swift
//
//  Created by Mahamudul on 22/10/20.
//

import Foundation

final class MyUserDefaults {
    
    static let shared = MyUserDefaults()
    
    static let name = "name"
    static let schoolName = "SchoolName"
    
    var defaults: UserDefaults? = nil
    
    init() {
        defaults = UserDefaults.standard
    }
    
    //Save data
    func saveInt(key: String, value: Int){
        defaults?.setValue(value, forKey: key)
    }
    
    func saveString(key: String, value: String){
        defaults?.setValue(value, forKey: key)
    }
    
    func saveBool(key: String, value: Bool){
        defaults?.setValue(value, forKey: key)
    }

    
    //Get Data
    func getInt(key: String)-> Int?{
        return defaults?.integer(forKey: key)
    }
    
    func getString(key: String)-> String?{
        return defaults?.string(forKey: key)
    }
    
    func getBool(key: String)-> Bool{
        return defaults!.bool(forKey: key)
    }
    
}

