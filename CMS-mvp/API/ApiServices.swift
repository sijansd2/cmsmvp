
import Foundation
import UIKit
import Alamofire

final class ApiServices{
    
    //Singletone
    static let shared = ApiServices()
    
    //api endpoints
    let allDataApi = "/index1.php"

    
    //MARK:-Get Events Detail API
    func getAllaaData(
        _ vc: UIViewController,
        completion: @escaping (Result<ScoreResponse>) -> ()){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            //show loader
            LoadingView.shared.showLoader(sender: vc)
        })
        
        
        let url = URL(string: Constants.baseUrl + allDataApi)!
        
        var request = URLRequest(url: url)
                request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
            
            guard let data = data else {
                debugPrint(error!)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    //hide loader
                    LoadingView.shared.hideLoader(sender: vc)
                })
                return
            }
            
            do{
                let response = try JSONDecoder().decode(ScoreResponse.self, from: data)
                debugPrint(data)
                
                DispatchQueue.main.async {
                    completion(.success(response))
                }
            }
            catch let jsonError{
                debugPrint(jsonError)
                DispatchQueue.main.async {
                    completion(.failure(jsonError))
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                //hide loader
                LoadingView.shared.hideLoader(sender: vc)
            })
        }

        task.resume()
    }
    
    
    
    func getAllData(
        _ vc: UIViewController,
        completion: @escaping (Result<ScoreResponse>) -> ()){
        
//        guard MyReachability.isConnected() else {
//            vc!.showAlert(title: "Error", msg: Constants.NO_INTERNET)
//            return
//        }
        
        LoadingView.shared.showLoader(sender: vc)
        
        let apiUrl = Constants.baseUrl + allDataApi
        
        
        AF.request(apiUrl)
            .responseJSON { response in
                     
                LoadingView.shared.hideLoader(sender: vc)
                                
                 do{
                    let response = try JSONDecoder().decode(ScoreResponse.self, from: response.data!)
                    
                     completion(.success(response))
                 }
                 catch let jsonError{
                    debugPrint(jsonError)
                     completion(.failure(jsonError))
                 }
                
            }
    }
}
