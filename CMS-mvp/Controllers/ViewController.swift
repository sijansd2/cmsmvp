//
//  ViewController.swift
//  CMS-mvp
//
//  Created by Mahamudul on 14/3/21.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var lb_player_name: UILabel!
    @IBOutlet weak var lb_school_name: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var response: ScoreResponse? = nil
    @IBOutlet weak var img_user: UIImageView!
    static var profileImg: UIImage? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        loadAllData()
        
        
        let name = MyUserDefaults.shared.getString(key: MyUserDefaults.name)
        let scName = MyUserDefaults.shared.getString(key: MyUserDefaults.schoolName)
        
        if name == nil{
            MyUserDefaults.shared.saveString(key: MyUserDefaults.name, value: "John Scott")
        }
        
        if scName == nil{
            MyUserDefaults.shared.saveString(key: MyUserDefaults.schoolName, value: "Conestoga High School")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let name = MyUserDefaults.shared.getString(key: MyUserDefaults.name)
        let scName = MyUserDefaults.shared.getString(key: MyUserDefaults.schoolName)
        
        lb_player_name.text = name
        lb_school_name.text = scName
        
        if let image = getSavedImage(named: "fileName") {
            // do something with image
            img_user.image = image
            img_user.layer.cornerRadius = 10
            img_user.clipsToBounds = true
        }
    }
    
    
    @IBAction func addGameBtnClicked(_ sender: Any) {
        gotoAddGameVC()
    }
    
    @IBAction func settingsBtnClicked(_ sender: Any) {
        gotoSettingsVC()
    }
    

}


extension ViewController{
    
    func gotoAddGameVC(){
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "addGameSID") as! AddGameViewController
        
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    
    func gotoSettingsVC(){
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "settingsSID") as! SettingsViewController
        
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func loadAllData(){
        
        ApiServices.shared.getAllData(self) { response in
            
            switch response{
            case.success(let response):
                self.response = response
                self.tableView.reloadData()
                break
                
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    func getSavedImage(named: String) -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
}


extension ViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.response?.myData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifire, for: indexPath) as? TableViewCell
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        let sortedData = self.response?.myData!.sorted(by: { (a1, a2) -> Bool in
            return dateFormatter.date(from: a1.date!)! > dateFormatter.date(from: a2.date!)!
        })
        let data = sortedData![indexPath.row]
        
        cell?.configure(data)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    
}


extension UIViewController{
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
