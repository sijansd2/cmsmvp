//
//  AddGameViewController.swift
//  CMS-mvp
//
//  Created by Mahamudul on 22/3/21.
//

import UIKit

class AddGameViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var tf_playingwith: UITextField!
    @IBOutlet weak var location: UITextField!
    @IBOutlet weak var date: UITextField!
    @IBOutlet weak var starttime: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        
        self.tf_playingwith.delegate = self
        self.location.delegate = self
        self.date.delegate = self
        self.starttime.delegate = self

        // Do any additional setup after loading the view.
        
        
    }
    
    @IBAction func backClicked(_ sender: Any) {
        backVC()
    }
}


extension AddGameViewController{
    
    func backVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.switchBasedNextTextField(textField)
        return true
    }

    private func switchBasedNextTextField(_ textField: UITextField) {
        switch textField {
        case self.tf_playingwith:
            self.location.becomeFirstResponder()
        case self.location:
            self.date.becomeFirstResponder()
        case self.date:
            self.starttime.becomeFirstResponder()
        default: break

        }
    }
}
