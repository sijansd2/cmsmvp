//
//  SettingsViewController.swift
//  CMS-mvp
//
//  Created by Mahamudul on 23/3/21.
//

import UIKit

class SettingsViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var tf_name: UITextField!
    @IBOutlet weak var tf_school_name: UITextField!
    @IBOutlet weak var lb_school_title: UILabel!
    @IBOutlet weak var lb_school_title_land: UILabel!
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var chooseBuuton: UIButton!
    var imagePicker = UIImagePickerController()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
        
        let name = MyUserDefaults.shared.getString(key: MyUserDefaults.name)
        let scName = MyUserDefaults.shared.getString(key: MyUserDefaults.schoolName)
        
        tf_name.text = name
        tf_school_name.text = scName
        lb_school_title.text = scName
        lb_school_title_land.text = scName
        
        if let image = getSavedImage(named: "fileName") {
            // do something with image
            
            imageView.image = image
            imageView.layer.cornerRadius = 30
            imageView.clipsToBounds = true
        }
    }
    
    @IBAction func backClicked(_ sender: Any) {
        backVC()
    }
    
   
    @IBAction func btnClicked() {

        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")

            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false

            present(imagePicker, animated: true, completion: nil)
        }
    }

    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.dismiss(animated: true, completion: { () -> Void in

        })
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        
        let success = saveImage(image: image)
        
        if let image = getSavedImage(named: "fileName") {
            // do something with image
            
            imageView.image = image
            imageView.layer.cornerRadius = 30
            imageView.clipsToBounds = true
        }
    }
    
    
    @IBAction func updateClicked(_ sender: Any) {
        
        MyUserDefaults.shared.saveString(key: MyUserDefaults.name, value: tf_name.text ?? "")
        MyUserDefaults.shared.saveString(key: MyUserDefaults.schoolName, value: tf_school_name.text ?? "")
        
        lb_school_title.text = tf_school_name.text ?? ""
        lb_school_title_land.text = tf_school_name.text ?? ""
    }
}


extension SettingsViewController{
    
    func backVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func saveImage(image: UIImage) -> Bool {
        guard let data = image.jpegData(compressionQuality: 1) ?? image.pngData() else {
            return false
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        do {
            try data.write(to: directory.appendingPathComponent("fileName.png")!)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    
    
    
    func getSavedImage(named: String) -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
    
}
