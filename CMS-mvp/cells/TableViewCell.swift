//
//  TableViewCell.swift
//  CMS-mvp
//
//  Created by Mahamudul on 14/3/21.
//

import UIKit
import Charts

class TableViewCell: UITableViewCell, ChartViewDelegate {
    
    static let identifire = "TableViewCell"
    
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var tv_max_value: UILabel!
    @IBOutlet weak var tv_max_value_land: UILabel!
    @IBOutlet weak var tv_yMax_value: UILabel!
    @IBOutlet weak var img_value_bg: UIImageView!
    @IBOutlet weak var img_value_bg_land: UIImageView!
    
    
    //var barChart = BarChartView()
    
    @IBOutlet weak var chartContainer: BarChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        chartContainer.delegate = self
    }
    
    public func configure(_ data: MyDatum?){
        
        
        location.text = data?.playedWith
        date.text = data?.date
        tv_max_value.text = String(data?.maxData ?? 0)
        tv_yMax_value.text = String(data?.maxData ?? 0)
        tv_max_value_land.text = String(data?.maxData ?? 0)
        
        if data?.maxData ?? 0 <= 2{
            img_value_bg.image = UIImage(named: "greenBg")
            img_value_bg_land.image = UIImage(named: "greenBg")
        }
        else if data?.maxData ?? 0 <= 5{
            img_value_bg.image = UIImage(named: "orangeBg")
            img_value_bg_land.image = UIImage(named: "orangeBg")
        }
        else{
            img_value_bg.image = UIImage(named: "redBg")
            img_value_bg_land.image = UIImage(named: "redBg")
        }
        
        
        var entries = [BarChartDataEntry]()
        let count = data?.timePeriod?.count ?? 0
        
        for i in 0..<count {
            let graphData = data?.timePeriod?[i].data ?? "0"
            let yValue = Double(graphData)
            entries.append(BarChartDataEntry(x: Double(i), y: yValue!))
        }
        
        let set = BarChartDataSet(entries: entries)
        //set.colors = ChartColorTemplates.joyful()
        set.drawValuesEnabled = false
        set.valueTextColor = UIColor.clear
        set.colors = [UIColor.black]
        
        let data = BarChartData(dataSet: set)
        
        //chartContainer.leftAxis.drawGridLinesEnabled = false
        chartContainer.data = data
        
        
        
        chartContainer.rightAxis.enabled = false
        chartContainer.xAxis.drawGridLinesEnabled = false
        chartContainer.xAxis.drawAxisLineEnabled = false
        chartContainer.xAxis.drawGridLinesBehindDataEnabled = false
        chartContainer.leftAxis.drawAxisLineEnabled = false
        chartContainer.pinchZoomEnabled = false
        chartContainer.leftAxis.enabled = false
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
